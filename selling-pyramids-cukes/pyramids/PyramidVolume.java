// Compute volume of pyramid in meters
// RWHasker, for SE 1011

package pyramids;

import java.util.Scanner;

public class PyramidVolume implements AbstractMain
{
  public void abstractMain(String[] args) {
    final double VOLUME_CONVERSION = 0.814; // for pyramids with slope of 52 degrees

    Scanner in = new Scanner(System.in);

    System.out.print("Enter the height of the pyramid in meters: ");
    double height = in.nextDouble();

    double volume = VOLUME_CONVERSION * height * height * height;

    System.out.println();

    System.out.println("A pyramid " + height + " meters high will take "
                       + volume
                       + " cubic meters of rock.");
    }

  public static void main(String[] args) {
    PyramidVolume pv = new PyramidVolume();
    pv.abstractMain(args);
  }
}
