// an interface for classes with a main method where the
//   main has been refactored to call abstractMain on an object
package pyramids;

public interface AbstractMain {
  void abstractMain(String[] args);
  default void abstractMain() {
    String[] empty = {};
    abstractMain(empty);
  }
}
