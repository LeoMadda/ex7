Feature: first set of sales

Scenario: 18 foot pyramid
  When I sell a pyramid 18 meters high
  Then the system will prompt for a height
  And the result for 18.0 meters will be 4747.248 cubic meters
