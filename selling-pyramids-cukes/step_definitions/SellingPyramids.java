package step_definitions;

import cucumber.api.java8.En;
import cucumber.runtime.java8.LambdaGlueBase;
import cucumber.api.java8.En;
import cucumber.api.PendingException;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import static org.junit.Assert.*;

import pyramids.*;

public class SellingPyramids implements En {

  private String[] outputLinesFromMainFor(AbstractMain mainObject,
                                          String inputText) {
    ByteArrayOutputStream capture = new ByteArrayOutputStream();
    PrintStream ps = new PrintStream(capture);
    PrintStream savedSystemOutput = System.out;
    System.setOut(ps);
    
    InputStream is = null;
    try {
      is  = new ByteArrayInputStream(inputText.getBytes("UTF-8"));
    } catch (UnsupportedEncodingException e) {
      System.err.println("Fatal error: " + e);
      System.exit(1);
    }
    InputStream savedSystemInput = System.in;
    System.setIn(is);
    
    mainObject.abstractMain();

    // Put things back
    System.out.flush();
    System.setOut(savedSystemOutput);
    System.setIn(savedSystemInput);

    String lines[] = capture.toString().split("[\r\n]+");

    return lines;
  }

  private String inputHeight = null;
  private String[] resultLines = null;

  public SellingPyramids() {
    When("^I sell a pyramid ([0-9]+.?[0-9]*) meters high$", (Double height) -> {
        // this must appear first
        inputHeight = height.toString();
        resultLines = outputLinesFromMainFor(new PyramidVolume(), inputHeight);
      });

    Then("^the system will prompt for a height", () -> {
        assertTrue(resultLines.length > 0);
        assertEquals("Enter the height of the pyramid in meters: ", resultLines[0]);
      });

    Then("^the result for ([0-9]+.?[0-9]*) meters will be ([0-9]+.?[0-9]*) cubic meters$",
         (String targetHeight, String targetVolume) -> {
           assertTrue(resultLines.length > 1);
           // matching the text:
           // A pyramid 18.0 meters high will take 4747.248 cubic meters of rock.
           String targetText = 
             "A pyramid " + targetHeight + " meters high will take " 
             + targetVolume + " cubic meters of rock.";
           assertEquals(targetText, resultLines[1]);
         });
  }
}
